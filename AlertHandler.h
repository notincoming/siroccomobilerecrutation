//
//  AlertHandler.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^completion)(void);

@interface AlertHandler : NSObject


-(void)alertHandling:(NSString *)title withMessage:(NSString *)message withController:(UIViewController *)controller withCompletion:completion;

@end
