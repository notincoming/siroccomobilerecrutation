//
//  News+CoreDataProperties.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "News+CoreDataProperties.h"

@implementation News (CoreDataProperties)

@dynamic data;
@dynamic encoding;
@dynamic mimeType;
@dynamic link;
@dynamic imageData;
@dynamic imageLink;
@dynamic imageMimeType;
@dynamic imageEncoding;
@dynamic title;
@dynamic publicationDate;
@dynamic newsID;

@end
