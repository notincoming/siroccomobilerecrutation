//
//  TableViewController.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import "TableViewController.h"
#import "TableViewCell.h"
#import "AppDelegate.h"

@interface TableViewController ()
{
    AppDelegate* delegate;
    int scrollOffset;
    int activityLabelHeight;
    BOOL isOutOfBottom;
}
@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = [[UIApplication sharedApplication] delegate];
    scrollOffset = 100;
    activityLabelHeight = 20;
    
    isOutOfBottom = NO;
    self.archiveCheck = [[ArchiveCheck alloc] init];

    self.navigationController.navigationBar.translucent = NO;
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationItem.backBarButtonItem = backButtonItem;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [delegate.newsToShow count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TableViewCell *cell = (TableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"reuseMe" forIndexPath:indexPath];
    
    NSDictionary *objectToShow = [delegate.newsToShow objectAtIndex:indexPath.section];
    UIImage* image = [UIImage imageWithData:[objectToShow objectForKey:@"imageData"]];
    
    [cell.imageViewInternal setImage:image];

    
    cell.label.text = [objectToShow objectForKey:@"title"];
    cell.dateLabel.text = [self dateStringAfterFormatting:[objectToShow objectForKey:@"publicationDate"]];

    return cell;
}

- (NSString*)dateStringAfterFormatting:(NSString*)dateString{
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *regionLocale = [NSLocale currentLocale];
    
    [rfc3339DateFormatter setLocale:regionLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mmZZZZ"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
     NSDate *date = [rfc3339DateFormatter dateFromString:dateString];
    
    [rfc3339DateFormatter setDateFormat:@"dd.MM.yyyy, HH:mm"];
    
    NSString *expectedDate = [rfc3339DateFormatter stringFromDate:date];
    
    return expectedDate;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    float endFrame = self.tableView.frame.size.height + scrollView.contentOffset.y;
    
    if(endFrame >= scrollView.contentSize.height + scrollOffset){
        
        isOutOfBottom = YES;
        
        [self basicActivityIndicatorSetup];
        
        self.refreshControlView = [[UIView alloc] initWithFrame:CGRectMake(0, scrollView.contentSize.height, scrollView.contentSize.width, scrollOffset + self.activityIndicator.frame.size.height)];
        
        self.refreshControlView.backgroundColor = [UIColor clearColor];
        
        [scrollView addSubview:self.refreshControlView];
        
        [self.activityIndicator setFrame:CGRectMake(scrollView.center.x - self.activityIndicator.frame.size.width / 2,
                                                    self.refreshControlView.frame.size.height / 2 - self.activityIndicator.frame.size.height / 2,
                                                    self.activityIndicator.frame.size.width,
                                                    self.activityIndicator.frame.size.height)];
        
        [self.refreshControlView addSubview:self.activityIndicator];
        
        [self basicActivityLabelSetup];
        
        self.activityLabel.text = @"Ładowanie...";
        
        [self.refreshControlView addSubview:self.activityLabel];
        
        [self.activityIndicator startAnimating];
        
        [self.archiveCheck checkAvailableArchive];
        
    }
}

- (void)basicActivityLabelSetup{
   
    self.activityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                   self.activityIndicator.center.y + self.activityIndicator.frame.size.height / 2 + activityLabelHeight / 4,
                                                                   self.tableView.frame.size.width, activityLabelHeight)];
    [self.activityLabel setBackgroundColor:[UIColor clearColor]];
    [self.activityLabel setFont:[UIFont fontWithName:[[UIFont familyNames] firstObject] size:17.0f]];
    
    [self.activityLabel setTextColor:[UIColor blackColor]];
    
    [self.activityLabel setTextAlignment:NSTextAlignmentCenter];

}
- (void)basicActivityIndicatorSetup{
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.5f, 1.5f);
    
    self.activityIndicator.transform = transform;
    
}
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    
    if(isOutOfBottom){
     
      [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + 2 * scrollOffset)];
      [scrollView setContentOffset:scrollView.contentOffset animated:YES];
      [scrollView setUserInteractionEnabled:NO];
      
      isOutOfBottom = NO;
     
      if(self.archiveCheck.availableArchives){
            
         [self.archiveCheck archiveNewsFetch];
         
         
          [UIView animateWithDuration:1.0 animations:^{
              
              [self.activityIndicator setAlpha:0];
              [self.activityLabel setAlpha:0];
          }];
          
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
              
             
              [self.activityIndicator stopAnimating];
              [self.refreshControlView removeFromSuperview];

              [UIView animateWithDuration:0.1 animations:^{
                  
                  [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height - 2 * scrollOffset)];
              }];
              
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                  
                  [self.tableView reloadData];

                  [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y + 2 * scrollOffset) animated:YES];
                  
                  [scrollView setUserInteractionEnabled:YES];
              });
              

          });
         
      }
      else{
          
          self.activityLabel.text = @"Brak wiadomości archiwalnych";
          
          [UIView animateWithDuration:1.0 animations:^{
              
              [self.activityIndicator setAlpha:0];
          }];
          
          [UIView animateWithDuration:0.3 animations:^{
              
              [self.refreshControlView setBackgroundColor:[UIColor greenColor]];
          }];
              
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
             
              [UIView animateWithDuration:0.7 animations:^{
                  
                  [self.refreshControlView setAlpha:0];
              }];
          });
          
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
          
              [self.activityIndicator stopAnimating];
              [self.refreshControlView removeFromSuperview];
          
              [UIView animateWithDuration:0.3 animations:^{
              
                  [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height - 2 * scrollOffset)];
              }];
           
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                  
                  [scrollView setUserInteractionEnabled:YES];
              });
          
          });
        
      }
     
    }
}

- (void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useNetworkNotification:)
     name:@"NetworkNotification"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useUpdateNotification:)
     name:@"UpdateNotification"
     object:nil];
 
  // UPDATE - CZESC DEMONSTRACYJNA
    
  /*  _updateDetector = [[UpdateDetector alloc] init];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
       
        [_updateDetector checkForUpdates];
    }); */
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)useNetworkNotification:(NSNotification*)notification{
    
    BOOL networkInformation = [[notification.userInfo objectForKey:@"isNetwork"] boolValue];
    
    if(!networkInformation){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [delegate.alertHandler alertHandling:@"Błąd sieci" withMessage:@"Brak sieci!" withController:self withCompletion:nil];
            
        });
        
    }
}

- (void)useUpdateNotification:(NSNotification*)notification{
    
    BOOL updateInformation = [[notification.userInfo objectForKey:@"endOfUpdate"] boolValue];
    
    if(!updateInformation){
        
        NSLog(@"No Updates");
    }
    else{
        
        NSLog(@"Updates are here");
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    delegate.objectToShow = [[delegate.newsToShow objectAtIndex:indexPath.section] copy];
}

@end
