//
//  UpdateDetector.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 02.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsFetch.h"

@interface UpdateDetector : NSObject

@property (assign) BOOL isUpdate;
@property (assign) BOOL endOfUpdate;

@property (assign) int numberOfAvailableUpdates;

@property (nonatomic, strong) NSMutableArray* updates;
@property (nonatomic, strong) NewsFetch* news;

- (id)init;
- (void)checkForUpdates;
@end
