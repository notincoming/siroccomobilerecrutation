//
//  ViewController.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InitialViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;

@end

