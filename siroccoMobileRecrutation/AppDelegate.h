//
//  AppDelegate.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AlertHandler.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) AlertHandler* alertHandler;
@property (nonatomic, strong) NSArray* fetchedNews;
@property (nonatomic, strong) NSArray* newsToShow;

@property (assign) int highestPriority;
@property (assign) BOOL isNetwork;

@property (nonatomic, strong) NSDictionary* objectToShow;

- (void)networkCheck;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

