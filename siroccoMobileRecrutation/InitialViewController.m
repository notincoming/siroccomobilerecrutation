//
//  ViewController.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import "InitialViewController.h"
#import "AppDelegate.h"
#import "NewsFetch.h"
#import "News.h"

@interface InitialViewController ()
{
    AppDelegate* delegate;
    NewsFetch* newsFetch;
    News* news;
    NSURL* newsURL;
    NSMutableArray* temporaryNewsToShow;
    int maximumNumberOfNews;
    int numberOfRequests;
    int numberOfDoneRequests;
}
@end

@implementation InitialViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self basicActivityIndicatorSetup];
    
    [self.view addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];

    maximumNumberOfNews = 10;
    
    numberOfRequests = 0;
    
    numberOfDoneRequests = 0;
    
    delegate = [[UIApplication sharedApplication] delegate];
    
    newsURL = [NSURL URLWithString:@"http://sirocco.home.pl/apkftp/testy/xmls/news"];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useNetworkNotification:)
     name:@"NetworkNotification"
     object:nil];
    
    temporaryNewsToShow = [[NSMutableArray alloc] init];
    
    delegate.newsToShow = [[NSArray alloc] init];
    
    [self getTheHighestPriorityFromCoreData];
    
    if(!delegate.isNetwork){
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [delegate.alertHandler alertHandling:@"Błąd sieci" withMessage:@"Brak sieci!" withController:self withCompletion:^{
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self fetchNewsFromCoreData];
                });
                
            }];
        });
    }
    else{
        

        [self fetchNewsAllocation];
        
        [self coreNewsAllocation];
    }
}

- (void)getTheHighestPriorityFromCoreData{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    NSPredicate* predicate;
    NSError* error;
    NSArray *result;
    
    predicate = [NSPredicate predicateWithFormat:@"title != %@", nil];
    [request setPredicate:predicate];
    
    result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(!error){
        
        if([result count] != 0){
            
            int maxValue = 0;
            
            for (int i = 0; i  < [result count]; i++){
                
                news = [result objectAtIndex:i];
                
                if([news.newsID intValue] >= maxValue)
                    maxValue = [news.newsID intValue];
            }
            
            delegate.highestPriority = maxValue;
        }
        else{
            
            delegate.highestPriority = 0;
        }
    }
    else{
        
        NSLog(@"Error during fetching from CoreData at InitialViewController -> getTheHighestPriority...");
    }
}
- (void)fetchNewsFromCoreData{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    NSPredicate* predicate;
    NSError* error;
    NSArray *result;
    
    predicate = [NSPredicate predicateWithFormat:@"title != %@", nil];
    [request setPredicate:predicate];
    
    result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(!error){
        
        if([result count] != 0){
            
            if([result count] > maximumNumberOfNews){
                
                [self getTheHighestPriorityFromCoreData];
                
                for(int i = 0; i < maximumNumberOfNews; i++){
                    
                    BOOL availabilityFlag = NO;
                    
                    do{
                    
                        predicate = [NSPredicate predicateWithFormat:@"newsID == %@", [NSNumber numberWithInt:delegate.highestPriority]];
                    
                        [request setPredicate:predicate];
                    
                        result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
                    
                        if(!error){
                        
                            if([result count] != 0){
                            
                                availabilityFlag = YES;
                                news = [result firstObject];
                                NSDictionary *temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     news.newsID,@"newsID",
                                                                     news.data,@"data",
                                                                     news.imageData,@"imageData",
                                                                     news.publicationDate,@"publicationDate",
                                                                     news.title,@"title",
                                                                     news.link,@"link",
                                                                     news.imageLink,@"imageLink",
                                                                     news.mimeType,@"mimeType",
                                                                     news.imageMimeType,@"imageMimeType",
                                                                     news.encoding,@"encoding",
                                                                     news.imageEncoding,@"imageEncoding",
                                                                     nil];
                                
                                [temporaryNewsToShow addObject:[temporaryDictionary copy]];
                                delegate.highestPriority--;
                            }
                            else{
                            
                                availabilityFlag = NO;
                                delegate.highestPriority--;
                            }
                        
                        }
                        else{
                        
                        NSLog(@"Error during CoreData fetching at InitialViewController -> fetchNewsFromCoreData -> by newsID predicate");
                        }
                    
                    }while(!availabilityFlag);
                }
            }
            else{
                
                for(int i = (int)([result count] - 1); i > 0; i--){
                    
                    BOOL availabilityFlag = NO;
                    
                    do{
                        
                        predicate = [NSPredicate predicateWithFormat:@"newsID == %@", [NSNumber numberWithInt:delegate.highestPriority]];
                        
                        [request setPredicate:predicate];
                        
                        result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
                        
                        if(!error){
                            
                            if([result count] != 0){
                                
                                availabilityFlag = YES;
                                news = [result firstObject];
                                NSDictionary *temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     news.newsID,@"newsID",
                                                                     news.data,@"data",
                                                                     news.imageData,@"imageData",
                                                                     news.publicationDate,@"publicationDate",
                                                                     news.title,@"title",
                                                                     news.link,@"link",
                                                                     news.imageLink,@"imageLink",
                                                                     news.mimeType,@"mimeType",
                                                                     news.imageMimeType,@"imageMimeType",
                                                                     news.encoding,@"encoding",
                                                                     news.imageEncoding,@"imageEncoding",
                                                                     nil];
                                
                                [temporaryNewsToShow addObject:[temporaryDictionary copy]];
                                delegate.highestPriority--;
                            }
                            else{
                                
                                availabilityFlag = NO;
                                delegate.highestPriority--;
                            }
                            
                        }
                        else{
                            
                            NSLog(@"Error during CoreData fetching at InitialViewController -> fetchNewsFromCoreData -> by newsID predicate");
                        }
                        
                    }while(!availabilityFlag);
                }
            }
            
            delegate.newsToShow = [temporaryNewsToShow copy];
            [self pushSegue];
        }
        else{
            
            [self pushSegue];
        }
    }
    else{
        
        NSLog(@"Error occured during fetching from CoreData at InitialViewController -> fetchNewsFromCoreData");
    }
    
    
}
- (void)fetchNewsAllocation{
    
    newsFetch = [[NewsFetch alloc] initWithURL:newsURL];
    delegate.fetchedNews= [NSArray arrayWithArray:[newsFetch.feeds copy]];
    
}

- (void)coreNewsAllocation{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    NSPredicate* predicate;
    NSError* error;
    NSArray *result;
    
    for(int i = 0; i < (int)[delegate.fetchedNews count]; i++){
        
        predicate = [NSPredicate predicateWithFormat:@"title == %@", [[delegate.fetchedNews objectAtIndex:i] objectForKey:@"title"]];
        
        [request setPredicate:predicate];
        
        result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
        
        if(!error){
            
            if([result count] != 0){
                
                news = [result firstObject];
                
                //add news from core
                NSDictionary *temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                     news.newsID,@"newsID",
                                                     news.data,@"data",
                                                     news.imageData,@"imageData",
                                                     news.publicationDate,@"publicationDate",
                                                     news.title,@"title",
                                                     news.link,@"link",
                                                     news.imageLink,@"imageLink",
                                                     news.mimeType,@"mimeType",
                                                     news.imageMimeType,@"imageMimeType",
                                                     news.encoding,@"encoding",
                                                     news.imageEncoding,@"imageEncoding",
                                                     nil];
                
                [temporaryNewsToShow addObject:[temporaryDictionary copy]];
            }
            else{
                
                NSLog(@"There is no results in CoreData at InitialViewController -> coreNewsAllocation");

                numberOfRequests++;
                ++delegate.highestPriority;
                
                [self networkNewsRequestWithIndex:i withNextAvailableID:delegate.highestPriority];
            }
        }
        else{
            
            NSLog(@"Error during CoreData search in InitialViewController -> coreNewsAllocation");
            
        }
        
        if(numberOfDoneRequests == numberOfRequests){
            
            delegate.newsToShow = [temporaryNewsToShow copy];
            [self pushSegue];
        }
    }
    
    
}

- (void)networkNewsRequestWithIndex:(int)index withNextAvailableID:(int)newsID{
    
    
    NSDictionary* newsObject = [[delegate.fetchedNews objectAtIndex:index] copy];
    
    NSURL* url = [NSURL URLWithString:[newsObject objectForKey:@"link"]];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        
        if(![[newsObject objectForKey:@"imageLink"] isEqualToString:@"NO"])
            
            [self networkImageReqeustWithIndex:index withExternalData:data withExternalResponse:response withExternalError:error
             withNextAvailableID:newsID];
            
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(data != nil && response != nil && error == nil){
    
                    
                    News* cacheObject = [NSEntityDescription insertNewObjectForEntityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
                    cacheObject.newsID = [NSNumber numberWithInt:newsID];
                    cacheObject.link = [newsObject objectForKey:@"link"];
                    cacheObject.imageLink = [newsObject objectForKey:@"imageLink"];
                    cacheObject.title = [newsObject objectForKey:@"title"];
                    cacheObject.data = [data copy];
                    cacheObject.imageData = [NSData dataWithBytes:nil length:0];
                    cacheObject.mimeType = response.MIMEType;
                    cacheObject.encoding = response.textEncodingName;
                    cacheObject.publicationDate = [newsObject objectForKey:@"publicationDate"];
                    cacheObject.imageMimeType = @"NO";
                    cacheObject.imageEncoding = @"NO";
                    
                    
                    NSError* cacheError;
                    
                    const BOOL saveSuccess = [delegate.managedObjectContext save:&cacheError];
                    if(!cacheError && !saveSuccess){
                        
                        NSLog(@"Saving problem at InitialViewController -> networkNewsRequestWith...");
                    }
                    else{
                        
                        //save wiec dobrze
                        NSDictionary *temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                             cacheObject.newsID,@"newsID",
                                                             cacheObject.data,@"data",
                                                             cacheObject.imageData,@"imageData",
                                                             cacheObject.publicationDate,@"publicationDate",
                                                             cacheObject.title,@"title",
                                                             cacheObject.link,@"link",
                                                             cacheObject.imageLink,@"imageLink",
                                                             cacheObject.mimeType,@"mimeType",
                                                             cacheObject.imageMimeType,@"imageMimeType",
                                                             cacheObject.encoding,@"encoding",
                                                             cacheObject.imageEncoding,@"imageEncoding",
                                                             nil];
                        
                        [temporaryNewsToShow addObject:[temporaryDictionary copy]];
                        
                    }
                    
                    
                }
                else{
                    
                    NSLog(@"An error occured during internalDataSession at InitialViewController -> networkNewsRequestWith...");
                }
                
                ++numberOfDoneRequests;
                if(numberOfDoneRequests == numberOfRequests){
                    
                    delegate.newsToShow = [temporaryNewsToShow copy];
                    [self pushSegue];
                }
            });
            
        }
    }];
    
    
    [dataTask resume];
}

- (void)networkImageReqeustWithIndex:(int)index withExternalData:(NSData*)data withExternalResponse:(NSURLResponse*)response withExternalError:(NSError*)error withNextAvailableID:(int)newsID{
    
    NSDictionary* newsObject = [[delegate.fetchedNews objectAtIndex:index] copy];
    
    NSURL* urlImage = [NSURL URLWithString:[newsObject objectForKey:@"imageLink"]];
    NSURLRequest* requestInternal = [NSURLRequest requestWithURL:urlImage];
    
    
    NSURLSessionConfiguration *configInternal = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *sessionInternal = [NSURLSession sessionWithConfiguration:configInternal];

    NSURLSessionDataTask *dataTaskInternal = [sessionInternal dataTaskWithRequest:requestInternal completionHandler:^(NSData *dataInternal ,NSURLResponse *responseInternal ,NSError *errorInternal) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if(data != nil && response != nil && error == nil &&
               dataInternal != nil && responseInternal != nil && errorInternal == nil){
            
                
                News* cacheObject = [NSEntityDescription insertNewObjectForEntityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
                cacheObject.newsID = [NSNumber numberWithInt:newsID];
                cacheObject.link = [newsObject objectForKey:@"link"];
                cacheObject.imageLink = [newsObject objectForKey:@"imageLink"];
                cacheObject.title = [newsObject objectForKey:@"title"];
                cacheObject.data = [data copy];
                cacheObject.imageData = [dataInternal copy];
                cacheObject.mimeType = response.MIMEType;
                cacheObject.encoding = response.textEncodingName;
                cacheObject.publicationDate = [newsObject objectForKey:@"publicationDate"];
                cacheObject.imageMimeType = responseInternal.MIMEType;
                cacheObject.imageEncoding = responseInternal.textEncodingName;
                
                
                NSError* cacheError;
                
                const BOOL saveSuccess = [delegate.managedObjectContext save:&cacheError];
                if(!cacheError && !saveSuccess){
                    
                    NSLog(@"Saving problem at InitialViewController -> networkImageRequestWith...");
                    
                }
                else{
                    
                    NSDictionary *temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                         cacheObject.newsID,@"newsID",
                                                         cacheObject.data,@"data",
                                                         cacheObject.imageData,@"imageData",
                                                         cacheObject.publicationDate,@"publicationDate",
                                                         cacheObject.title,@"title",
                                                         cacheObject.link,@"link",
                                                         cacheObject.imageLink,@"imageLink",
                                                         cacheObject.mimeType,@"mimeType",
                                                         cacheObject.imageMimeType,@"imageMimeType",
                                                         cacheObject.encoding,@"encoding",
                                                         cacheObject.imageEncoding,@"imageEncoding",
                                                         nil];
                    
                    [temporaryNewsToShow addObject:[temporaryDictionary copy]];
                    //save wiec dobrze
                }
                

            }
            else{
                
                NSLog(@"An error occured during internalDataSession at InitialViewController -> networkImageRequestWith...");
            }
            
            ++numberOfDoneRequests;
            if(numberOfDoneRequests == numberOfRequests){
                
                delegate.newsToShow = [temporaryNewsToShow copy];
                [self pushSegue];
            }
        });
    
    }];
    
    [dataTaskInternal resume];

}
- (void)useNetworkNotification:(NSNotification*)notification{
    
    BOOL networkInformation = [[notification.userInfo objectForKey:@"isNetwork"] boolValue];
    
    if(!networkInformation){
        
        [delegate.alertHandler alertHandling:@"Błąd sieci" withMessage:@"Połączenie zostało zerwane!" withController:self withCompletion:^{
        
            delegate.newsToShow = [temporaryNewsToShow copy];
            [self pushSegue];
        }];
    }
}

- (void)pushSegue{
    
    UIStoryboard* story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSArray *temporarySortedNews;
    
    temporarySortedNews = [delegate.newsToShow sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        
        NSNumber *first = [a objectForKey:@"newsID"];
        NSNumber *second = [b objectForKey:@"newsID"];
        
        return [second compare:first];
    }];
    
    delegate.newsToShow = [temporarySortedNews copy];
    
   /* for(int i = 0; i < [delegate.newsToShow count]; i++){
    
        NSLog(@"%@", [[delegate.newsToShow objectAtIndex:i] objectForKey:@"newsID"]);
    } */
    
    [[[[UIApplication sharedApplication] delegate] window] setRootViewController:[story instantiateViewControllerWithIdentifier:@"NavigationController"]];
    
    
}
- (void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)basicActivityIndicatorSetup{
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(2.5f, 2.5f);
    
    self.activityIndicator.transform = transform;
    
    [self.activityIndicator setFrame:CGRectMake(self.view.center.x - self.activityIndicator.frame.size.width / 2, self.view.center.y + self.activityIndicator.frame.size.height / 2, self.activityIndicator.frame.size.width, self.activityIndicator.frame.size.height)];
    
    [self.activityIndicator setTintColor:[UIColor blackColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
