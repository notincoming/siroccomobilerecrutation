//
//  News+CoreDataProperties.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "News.h"

NS_ASSUME_NONNULL_BEGIN

@interface News (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *data;
@property (nullable, nonatomic, retain) NSString *encoding;
@property (nullable, nonatomic, retain) NSString *mimeType;
@property (nullable, nonatomic, retain) NSString *link;
@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, retain) NSString *imageLink;
@property (nullable, nonatomic, retain) NSString *imageMimeType;
@property (nullable, nonatomic, retain) NSString *imageEncoding;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString* publicationDate;
@property (nullable, nonatomic, retain) NSNumber* newsID;

@end

NS_ASSUME_NONNULL_END
