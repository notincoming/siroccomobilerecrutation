//
//  NewsFetch.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import "NewsFetch.h"

@interface NewsFetch (){

    NSXMLParser* parser;
    NSString *element;

    NSDictionary *temporaryDictionary;
    NSMutableString* link;
    NSMutableString* imageLink;
    NSMutableArray* news;
}


@end

@implementation NewsFetch

-(id)initWithURL:(NSURL *)url{
    
    if(self = [super init]){
        
        news = [[NSMutableArray alloc] init];
        _feeds = [[NSArray alloc] init];
        
        parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        
        [parser setDelegate:self];
        [parser setShouldResolveExternalEntities:NO];
        [parser parse];
    
      
    }
    else{
        
        NSLog(@"Parse error in NewsFetch -> initWithURL");
    }
    
    return self;
}
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict{
    
    element = elementName;
    
    if([element isEqualToString:@"news"]){
        
        link = [[attributeDict objectForKey:@"url"] copy];
        link = (NSMutableString*)[link stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if([attributeDict objectForKey:@"thumbnail"]){
            
            imageLink = [[attributeDict objectForKey:@"thumbnail"] copy];
            imageLink = (NSMutableString*)[imageLink stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        else
            imageLink = [NSMutableString stringWithFormat:@"NO"];
        
        temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                               [[attributeDict objectForKey:@"title"] copy],@"title",
                               [[attributeDict objectForKey:@"pub_date"] copy],@"publicationDate",
                               [link copy],@"link",
                               [imageLink copy],@"imageLink",
                               nil];

        [news addObject:[temporaryDictionary copy]];
       
    }
    
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if([elementName isEqualToString:@"news_list"])
        self.feeds = [news copy];
    
}

@end
