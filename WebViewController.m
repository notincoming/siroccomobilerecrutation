//
//  WebViewController.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 02.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import "WebViewController.h"
#import "AppDelegate.h"

@interface WebViewController ()
{
    AppDelegate* delegate;
    float initialContentSize;
    float activityLabelHeight;
    
}
@end

@implementation WebViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    delegate = [[UIApplication sharedApplication] delegate];

    
    [self.webView setScalesPageToFit:YES];
    [self.webView setBackgroundColor:[UIColor clearColor]];
    
    activityLabelHeight = 20;
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    
    self.navigationItem.title = [delegate.objectToShow objectForKey:@"title"];
    
    [self.webView loadData:[delegate.objectToShow objectForKey:@"data"] MIMEType:[delegate.objectToShow objectForKey:@"mimeType"] textEncodingName:[delegate.objectToShow objectForKey:@"encoding"] baseURL:
     [NSURL absoluteURLWithDataRepresentation:[delegate.objectToShow objectForKey:@"data"] relativeToURL:nil]];
    
     [self.webView.scrollView setContentSize:CGSizeMake(self.webView.frame.size.width, self.webView.scrollView.contentSize.height)];
    
    [self basicActivityIndicatorSetup];
    [self basicActivityLabelSetup];
    
    [self.webView addSubview:self.activityIndicator];
    [self.webView addSubview:self.activityLabel];
    
    [self.activityIndicator startAnimating];
    
    initialContentSize = self.webView.scrollView.contentSize.height;
    
    [self trackTheContentSizeHeight];
}


- (void)trackTheContentSizeHeight{
    
    if(self.webView.scrollView.contentSize.height == initialContentSize){
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.05 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [self trackTheContentSizeHeight];
        });
    }
    else{
        
        [self.activityIndicator stopAnimating];
        [self.activityLabel setHidden:YES];
    }
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void)basicActivityIndicatorSetup{
    
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.5f, 1.5f);
    
    self.activityIndicator.transform = transform;
    
    [self.activityIndicator setFrame:CGRectMake(self.view.frame.size.width / 2 - self.activityIndicator.frame.size.width / 2,
                                                self.view.frame.size.height / 2 - self.activityIndicator.frame.size.height / 2,
                                                self.activityIndicator.frame.size.width,
                                                self.activityIndicator.frame.size.height)];
    
    [self.activityIndicator setTintColor:[UIColor blackColor]];
}

- (void)basicActivityLabelSetup{
    
    self.activityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                   self.activityIndicator.center.y + self.activityIndicator.frame.size.height / 2 + activityLabelHeight / 4,
                                                                   self.view.frame.size.width, activityLabelHeight)];
    [self.activityLabel setBackgroundColor:[UIColor clearColor]];
    [self.activityLabel setFont:[UIFont fontWithName:[[UIFont familyNames] firstObject] size:17.0f]];
    
    [self.activityLabel setTextColor:[UIColor blackColor]];
    
    [self.activityLabel setTextAlignment:NSTextAlignmentCenter];
    
    self.activityLabel.text = @"Ładowanie....";
    
    [self.activityLabel setHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self.activityIndicator removeFromSuperview];
    [self.activityLabel removeFromSuperview];

}


@end
