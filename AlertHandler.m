//
//  AlertHandler.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import "AlertHandler.h"

@implementation AlertHandler

- (void)alertHandling:(NSString*)title withMessage:(NSString*)message withController:(UIViewController*)controller withCompletion:(id)completion{
    
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* confirmation = [UIAlertAction actionWithTitle:@"OK!" style:UIAlertActionStyleDefault handler:completion];
    
    [alertController addAction:confirmation];
    
    [controller presentViewController:alertController animated:TRUE completion:nil];
}


@end
