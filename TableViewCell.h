//
//  TableViewCell.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewInternal;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;


@end
