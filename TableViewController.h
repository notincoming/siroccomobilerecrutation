//
//  TableViewController.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArchiveCheck.h"
#import "UpdateDetector.h"

@interface TableViewController : UITableViewController <UIScrollViewDelegate>

@property (nonatomic, strong) UIView* refreshControlView;
@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;
@property (nonatomic, strong) UILabel* activityLabel;
@property (nonatomic, strong) ArchiveCheck* archiveCheck;
@property (nonatomic, strong) UpdateDetector* updateDetector;

@end
