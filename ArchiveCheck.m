//
//  ArchiveCheck.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import "ArchiveCheck.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "News.h"

@interface ArchiveCheck (){
    
    int fetchingArchiveConstant;
    AppDelegate* delegate;
    NSMutableArray* temporaryNewsArray;
}
@end
@implementation ArchiveCheck

- (id)init{
    
    self = [super init];
    
    fetchingArchiveConstant = 3;
    delegate = [[UIApplication sharedApplication] delegate];
    
    return self;
}

- (void)checkAvailableArchive{
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    NSPredicate* predicate;
    NSError* error;
    NSArray *result;
    
    predicate = [NSPredicate predicateWithFormat:@"title != %@", nil];
    [request setPredicate:predicate];
    
    result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(!error){
        
        if([result count] != 0){
            
            self.availableArchives = YES;
            self.numberOfAvailableNews = (int)([result count] - [delegate.newsToShow count]);
            
            if(self.numberOfAvailableNews == 0)
                self.availableArchives = NO;
            
        }
        else{
            
            self.availableArchives = NO;
        }
    }
    else{
        
        NSLog(@"Error during CoreData fetching at ArchiveCheck -> checkAvailableArchive");
    }
}
- (void)archiveNewsFetch{
    
    temporaryNewsArray = [[NSMutableArray alloc] init];
    
    
    for(int i = 0; i < [delegate.newsToShow count]; i++){
        
        [temporaryNewsArray addObject:[[delegate.newsToShow objectAtIndex:i] copy]];
    }
    
    int lastNewsLoadedID = (int)[[[delegate.newsToShow lastObject] objectForKey:@"newsID"] intValue];
    --lastNewsLoadedID;
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    NSPredicate* predicate;
    NSError* error;
    NSArray *result;
    
    
    if(self.numberOfAvailableNews > fetchingArchiveConstant){
        
        for(int i = 0; i < fetchingArchiveConstant; i++){
            
            BOOL availabilityFlag = NO;
            
            do{
                predicate = [NSPredicate predicateWithFormat:@"newsID == %@", [NSNumber numberWithInt:lastNewsLoadedID]];
                [request setPredicate:predicate];
                result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
                
                if(!error){
                    
                    if([result count] != 0){
                        
                        availabilityFlag = YES;
                        News* news = [result firstObject];
                        
                        NSDictionary* temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                             news.newsID,@"newsID",
                                                             news.data,@"data",
                                                             news.imageData,@"imageData",
                                                             news.publicationDate,@"publicationDate",
                                                             news.title,@"title",
                                                             news.link,@"link",
                                                             news.imageLink,@"imageLink",
                                                             news.mimeType,@"mimeType",
                                                             news.imageMimeType,@"imageMimeType",
                                                             news.encoding,@"encoding",
                                                             news.imageEncoding,@"imageEncoding",
                                                             nil];
                        [temporaryNewsArray addObject:[temporaryDictionary copy]];
                        lastNewsLoadedID--;
                    }
                    else{
                        
                        availabilityFlag = NO;
                        lastNewsLoadedID--;
                    }
                }
                else{
                    
                    NSLog(@"Error during CoreData fetching at ArchiveCheck -> archiveNewsFetch -> fetchingArchiveConstant");
                }
            }while(!availabilityFlag);
            
        }
    }
    else{
        
        for(int i = 0; i < self.numberOfAvailableNews; i++){
            
            BOOL availabilityFlag = NO;
            
            do{
                predicate = [NSPredicate predicateWithFormat:@"newsID == %@", [NSNumber numberWithInt:lastNewsLoadedID]];
                [request setPredicate:predicate];
                result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
                
                if(!error){
                    
                    if([result count] != 0){
                        
                        availabilityFlag = YES;
                        News* news = [result firstObject];
                        
                        NSDictionary* temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                             news.newsID,@"newsID",
                                                             news.data,@"data",
                                                             news.imageData,@"imageData",
                                                             news.publicationDate,@"publicationDate",
                                                             news.title,@"title",
                                                             news.link,@"link",
                                                             news.imageLink,@"imageLink",
                                                             news.mimeType,@"mimeType",
                                                             news.imageMimeType,@"imageMimeType",
                                                             news.encoding,@"encoding",
                                                             news.imageEncoding,@"imageEncoding",
                                                             nil];
                        
                        [temporaryNewsArray addObject:[temporaryDictionary copy]];
                        lastNewsLoadedID--;
                    }
                    else{
                        
                        availabilityFlag = NO;
                        lastNewsLoadedID--;
                    }
                }
                else{
                    
                    NSLog(@"Error during CoreData fetching at ArchiveCheck -> archiveNewsFetch -> fetchingArchiveConstant");
                }
            }while(!availabilityFlag);
        }
    }
    
    delegate.newsToShow = [temporaryNewsArray copy];
}
@end
