//
//  WebViewController.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 02.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;
@property (nonatomic, strong) UILabel* activityLabel;

@end
