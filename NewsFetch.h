//
//  NewsFetch.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsFetch : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSArray* feeds;

- (id)initWithURL:(NSURL*)url;

@end
