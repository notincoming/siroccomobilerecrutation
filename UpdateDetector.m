//
//  UpdateDetector.m
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 02.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import "UpdateDetector.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "News.h"

@interface UpdateDetector (){

    AppDelegate* delegate;
    NSURL* newsURL;
    NSMutableArray* temporaryNewsToShow;
    int updateConstant;
    int numberOfRequests;
    int numberOfDoneRequests;
}
@end

@implementation UpdateDetector

- (id)init{
    
    self = [super init];
    
    delegate = [[UIApplication sharedApplication] delegate];
    newsURL = [NSURL URLWithString:@"http://sirocco.home.pl/apkftp/testy/xmls/news"];
    updateConstant = 3;
    
    
    
    return self;
}

- (void)checkForUpdates{
    
    if(self.numberOfAvailableUpdates != 0)
        [self performAnUpdate];
    
    else{
        
        self.news = [[NewsFetch alloc] initWithURL:newsURL];
        
        self.updates = [[NSMutableArray alloc] init];
        temporaryNewsToShow = [[NSMutableArray alloc] init];
        
        self.isUpdate = NO;
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entity];
        
        NSPredicate* predicate;
        NSError* error;
        NSArray *result;
        
        predicate = [NSPredicate predicateWithFormat:@"title != %@", nil];
        [request setPredicate:predicate];
        
        result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
        
        if(!error){
            
            if([result count] != 0){
                
                for(int i = (int)([self.news.feeds count] - 1); i > 0; i--){
                    
                    BOOL isAvailable = NO;
                    
                    for(int j = 0; j < [result count]; j++){
                        
                        News* cacheNews = [result objectAtIndex:j];
                        
                        if([[[self.news.feeds objectAtIndex:i] objectForKey:@"title"] isEqualToString:cacheNews.title]){
                        
                            isAvailable = NO;
                            break;
                        }
                        else
                            isAvailable = YES;
                        
                    }
                    
                    if(isAvailable){
                        
                        [self.updates addObject:[self.news.feeds objectAtIndex:i]];
                         self.isUpdate = YES;
                    }
                }
            }
            else{
                
                NSLog(@"No stored news at UpdateDetector -> checForUpdates");
            }
        }
        else{
            
            NSLog(@"Error during CoreData fetching at UpdateDetector -> checkForUpdates");
        }
        
         self.numberOfAvailableUpdates = (int)[self.updates count];
        
        if(self.isUpdate){
            
            for (int i = 0; i < [delegate.newsToShow count]; i++){
                
                [temporaryNewsToShow addObject:[[delegate.newsToShow objectAtIndex:i] copy]];
            }
            
            [self performAnUpdate];
        }
        else{
          
            self.endOfUpdate = NO;
            [self postUpdateNotification];
        }
        
    }
}

- (void)performAnUpdate{
    
    [self getTheHighestPriorityFromCoreData];
    
    if(self.numberOfAvailableUpdates > updateConstant){
        
        numberOfRequests = updateConstant;
        
        for(int i = (int)([self.updates count] -1); i > (int)([self.updates count] - 1 - updateConstant); i--){
            
            ++delegate.highestPriority;
            
            [self networkNewsRequestWithIndex:i withNextAvailableID:delegate.highestPriority];
            
        }
        
        self.numberOfAvailableUpdates -= updateConstant;
    }
    else{
        
        numberOfRequests = (int)[self.updates count];
        
        for(int i = (int)([self.updates count] -1); i > 0; i--){
            
            ++delegate.highestPriority;
            
            [self networkNewsRequestWithIndex:i withNextAvailableID:delegate.highestPriority];
            
        }
        
        self.isUpdate = NO;
        self.numberOfAvailableUpdates = 0;
        
    }
}

- (void)networkNewsRequestWithIndex:(int)index withNextAvailableID:(int)newsID{
    
    self.endOfUpdate = NO;
    
    NSDictionary* newsObject = [[self.updates objectAtIndex:index] copy];
    
    NSURL* url = [NSURL URLWithString:[newsObject objectForKey:@"link"]];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        
        if(![[newsObject objectForKey:@"imageLink"] isEqualToString:@"NO"])
            
            [self networkImageReqeustWithIndex:index withExternalData:data withExternalResponse:response withExternalError:error
                           withNextAvailableID:newsID];
        
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(data != nil && response != nil && error == nil){
                    
                    
                    News* cacheObject = [NSEntityDescription insertNewObjectForEntityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
                    cacheObject.newsID = [NSNumber numberWithInt:newsID];
                    cacheObject.link = [newsObject objectForKey:@"link"];
                    cacheObject.imageLink = [newsObject objectForKey:@"imageLink"];
                    cacheObject.title = [newsObject objectForKey:@"title"];
                    cacheObject.data = [data copy];
                    cacheObject.imageData = [NSData dataWithBytes:nil length:0];
                    cacheObject.mimeType = response.MIMEType;
                    cacheObject.encoding = response.textEncodingName;
                    cacheObject.publicationDate = [newsObject objectForKey:@"publicationDate"];
                    cacheObject.imageMimeType = @"NO";
                    cacheObject.imageEncoding = @"NO";
                    
                    
                    NSError* cacheError;
                    
                    const BOOL saveSuccess = [delegate.managedObjectContext save:&cacheError];
                    if(!cacheError && !saveSuccess){
                        
                        NSLog(@"Saving problem at UpdateDetector -> networkNewsRequestWith...");
                    }
                    else{
                        
                        //save wiec dobrze
                        NSDictionary *temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                             cacheObject.newsID,@"newsID",
                                                             cacheObject.data,@"data",
                                                             cacheObject.imageData,@"imageData",
                                                             cacheObject.publicationDate,@"publicationDate",
                                                             cacheObject.title,@"title",
                                                             cacheObject.link,@"link",
                                                             cacheObject.imageLink,@"imageLink",
                                                             cacheObject.mimeType,@"mimeType",
                                                             cacheObject.imageMimeType,@"imageMimeType",
                                                             cacheObject.encoding,@"encoding",
                                                             cacheObject.imageEncoding,@"imageEncoding",
                                                             nil];
                        
                        [temporaryNewsToShow addObject:[temporaryDictionary copy]];
                        
                    }
                    
                    
                }
                else{
                    
                    NSLog(@"An error occured during internalDataSession at InitialViewController -> networkNewsRequestWith...");
                }
                
                ++numberOfDoneRequests;
                [self.updates removeObjectAtIndex:index];
                
                if(numberOfDoneRequests == numberOfRequests){
                
                    self.endOfUpdate = YES;
                    delegate.newsToShow = [temporaryNewsToShow copy];
                    [self postUpdateNotification];
                }
                
            
            });
            
        }
    }];
    
    
    [dataTask resume];

}

- (void)networkImageReqeustWithIndex:(int)index withExternalData:(NSData*)data withExternalResponse:(NSURLResponse*)response withExternalError:(NSError*)error withNextAvailableID:(int)newsID{
    
    NSDictionary* newsObject = [[self.updates objectAtIndex:index] copy];
    
    NSURL* urlImage = [NSURL URLWithString:[newsObject objectForKey:@"imageLink"]];
    NSURLRequest* requestInternal = [NSURLRequest requestWithURL:urlImage];
    
    
    NSURLSessionConfiguration *configInternal = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *sessionInternal = [NSURLSession sessionWithConfiguration:configInternal];
    
    NSURLSessionDataTask *dataTaskInternal = [sessionInternal dataTaskWithRequest:requestInternal completionHandler:^(NSData *dataInternal ,NSURLResponse *responseInternal ,NSError *errorInternal) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if(data != nil && response != nil && error == nil &&
               dataInternal != nil && responseInternal != nil && errorInternal == nil){
                
                
                News* cacheObject = [NSEntityDescription insertNewObjectForEntityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
                cacheObject.newsID = [NSNumber numberWithInt:newsID];
                cacheObject.link = [newsObject objectForKey:@"link"];
                cacheObject.imageLink = [newsObject objectForKey:@"imageLink"];
                cacheObject.title = [newsObject objectForKey:@"title"];
                cacheObject.data = [data copy];
                cacheObject.imageData = [dataInternal copy];
                cacheObject.mimeType = response.MIMEType;
                cacheObject.encoding = response.textEncodingName;
                cacheObject.publicationDate = [newsObject objectForKey:@"publicationDate"];
                cacheObject.imageMimeType = responseInternal.MIMEType;
                cacheObject.imageEncoding = responseInternal.textEncodingName;
                
                
                NSError* cacheError;
                
                const BOOL saveSuccess = [delegate.managedObjectContext save:&cacheError];
                if(!cacheError && !saveSuccess){
                    
                    NSLog(@"Saving problem at InitialViewController -> networkImageRequestWith...");
                    
                }
                else{
                    
                    NSDictionary *temporaryDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                         cacheObject.newsID,@"newsID",
                                                         cacheObject.data,@"data",
                                                         cacheObject.imageData,@"imageData",
                                                         cacheObject.publicationDate,@"publicationDate",
                                                         cacheObject.title,@"title",
                                                         cacheObject.link,@"link",
                                                         cacheObject.imageLink,@"imageLink",
                                                         cacheObject.mimeType,@"mimeType",
                                                         cacheObject.imageMimeType,@"imageMimeType",
                                                         cacheObject.encoding,@"encoding",
                                                         cacheObject.imageEncoding,@"imageEncoding",
                                                         nil];
                    
                    [temporaryNewsToShow addObject:[temporaryDictionary copy]];
                    //save wiec dobrze
                }
                
                
            }
            else{
                
                NSLog(@"An error occured during internalDataSession at InitialViewController -> networkImageRequestWith...");
            }
            
            ++numberOfDoneRequests;
            [self.updates removeObjectAtIndex:index];
            
            if(numberOfDoneRequests == numberOfRequests){
                
                self.endOfUpdate = YES;
                delegate.newsToShow = [temporaryNewsToShow copy];
                [self postUpdateNotification];
            }
            
        });
        
    }];
    
    [dataTaskInternal resume];

}
- (void)getTheHighestPriorityFromCoreData{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"News" inManagedObjectContext:delegate.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    NSPredicate* predicate;
    NSError* error;
    NSArray *result;
    
    predicate = [NSPredicate predicateWithFormat:@"title != %@", nil];
    [request setPredicate:predicate];
    
    result = [delegate.managedObjectContext executeFetchRequest:request error:&error];
    
    if(!error){
        
        if([result count] != 0){
            
            int maxValue = 0;
            
            for (int i = 0; i  < [result count]; i++){
                
                News *cacheNews = [result objectAtIndex:i];
                
                if([cacheNews.newsID intValue] >= maxValue)
                    maxValue = [cacheNews.newsID intValue];
            }
            
            delegate.highestPriority = maxValue;
        }
        else{
            
            delegate.highestPriority = 0;
        }
    }
    else{
        
        NSLog(@"Error during fetching from CoreData at UpdateDetector -> getTheHighestPriority...");
    }
}

- (void)postUpdateNotification{
    
    NSString *notificationName = @"UpdateNotification";
    
    // endOfUpdate = NO -> no updates
    // endOfUpdate = YES -> fetching has been ended
    
    NSDictionary *information = [NSDictionary dictionaryWithObject:@(self.endOfUpdate) forKey:@"endOfUpdate"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:information];
    
}
@end
