//
//  ArchiveCheck.h
//  siroccoMobileRecrutation
//
//  Created by Macstorm on 01.08.2016.
//  Copyright © 2016 Me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArchiveCheck : NSObject

@property (assign) BOOL availableArchives;
@property (assign) int numberOfAvailableNews;

- (id)init;
- (void)checkAvailableArchive;
- (void)archiveNewsFetch;

@end
